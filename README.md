M017-Results

LiviaNET_T1 results: tissue probability map (4D) as well as hard thresholded binary label map (4D).
LiviaNET_T2 results: tissue probability map (4D) as well as hard thresholded binary label map (4D).
HyperDense-Net_T2T1 results: tissue probability map (4D) as well as hard thresholded binary label map (4D).

The randomly chosen eight Developing Human Connectome Neonates used for final hold out test:
379~480
- CC00379XX17_ses-120400
- CC00413XX09_ses-127100
- CC00415XX11_ses-127400
- CC00418BN14_ses-125300
- CC00421AN09_ses-126000
- CC00421BN09_ses-126100
- CC00422XX10_ses-127800
- CC00480XX11_ses-141609

2019-06-21 is the date T2-T1 experiment was conducted. Validated with DHCP subjects 303~367.
2019-07-18 is the date liviaNet T2 experiment was conducted. Validated with DHCP subjects 303~367.
2019-07-22 is the date LiviaNET T1 experiment was conducted. Validated with DHCP subjects 303~367. 
